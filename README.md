# SupportPhoneNumbers
Contains collection of support phone numbers

### How do I install it?
`install-package GreatCall.SupportPhoneNumbers`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/supportphonenumbers.wiki/wiki/).
